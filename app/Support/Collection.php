<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 19.01.2020 00:17
 */


namespace App\Support;



class Collection
{
    public $items = [];

    public function __construct($data)
    {
        if(is_array($data)) $this->items = $data;
        else $this->items[] = $data;
    }

    public function add($data, $column=null){
        if($column) $this->items[$column] = $data;
        else $this->items[] = $data;

        return $this;
    }

    public function unique()
    {
        $this->items = array_unique($this->items);

        return $this;
    }

    public function diff(array $mix)
    {
        $this->items = array_diff($this->items, $mix);

        return $this;
    }

    public function merge(array $mix)
    {
        $this->items = array_merge($this->items, $mix);

        return $this;
    }

    public function count()
    {
        return count($this->items);
    }

    public function map(callable $callback)
    {
        $keys = array_keys($this->items);

        $items = array_map($callback, $this->items, $keys);

        return new static(array_combine($keys, $items));
    }

    public function toArray()
    {
        return (array) $this->items;
    }
}