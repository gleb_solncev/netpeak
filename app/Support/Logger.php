<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:17
 */


namespace App\Support;


use App\Model\File;
use App\Model\File\TypeSave\SaveTXT;

class Logger
{

    private $start;
    protected $filename = 'log';

    protected $text, $file;

    public function __construct($start)
    {
        $this->file = new File;
        $this->start = $start;
    }

    public function print($text)
    {
        $this->text[] = $text . PHP_EOL;

        if(LOG_DISPLAY) {
            if (is_array($text)) print_r($text);
            else print $text;

            print PHP_EOL;
        }

    }

    public function saveData()
    {
        $this->file->setContent(implode(PHP_EOL, $this->text));
        $this->save();
    }

    public function exit($text)
    {
        $this->text[] = $text;
        $this->saveData();

        exit($text);
    }

    public function save()
    {
        if(LOG_FILE)
            $this->file->put(new SaveTXT, null, $this->filename);
    }
}