<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:08
 */


namespace App\Model;

use App\Model\File\Pattern\iPattern;
use App\Model\File\TypeSave\iSave;

class File extends BaseModel
{
    /**
     * @var String $content
     */
    protected $content;

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function parse(iPattern $pattern)
    {
        if (LOG_DISPLAY) print __METHOD__ . " | PATTERN >>> " . $pattern->pattern() . PHP_EOL;

        preg_match_all($pattern->pattern(), $this->content, $collection);
        return end($collection);
    }

    public function put(iSave $iSave, $path, $filename)
    {
        $basePath = realpath(getcwd() . "/" . $path);
        if (!$basePath) $basePath = $this->createDirs($path);

        return $iSave->doSave($basePath, $filename, $this->content);
    }

    public function createDirs($path): string
    {
        $ex_path = array_filter(explode(DS, $path));
        $fullPath = getcwd();

        foreach ($ex_path as $folder):
            $fullPath .= DS . $folder;

            if (!file_exists($fullPath)) mkdir($fullPath);
        endforeach;

        return realpath($fullPath);
    }

    public function get($path)
    {
        return file_get_contents($path);
    }
}