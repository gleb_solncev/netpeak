<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:09
 */


namespace App\Model\File\Pattern;


interface iPattern
{
    public function pattern();
}