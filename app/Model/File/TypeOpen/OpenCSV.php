<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:15
 */


namespace App\Model\File\TypeOpen;


class OpenCSV implements iOpen
{

    public function open($path)
    {
        if (($handle = fopen($path, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $link = reset($data);
                unset($data[0]);
                $count = null;
                foreach ($data as $cell):
                    $count[] = count(json_decode($cell, true));
                endforeach;
                $result[] = compact('link', 'count');
            }
            fclose($handle);
        }


        return $result;
    }
}