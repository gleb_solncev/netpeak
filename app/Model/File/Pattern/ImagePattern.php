<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:10
 */


namespace App\Model\File\Pattern;


class ImagePattern implements iPattern
{
    public function pattern()
    {
        return '/<img src=\"(.*?[^\"]+)\"/';
    }
}