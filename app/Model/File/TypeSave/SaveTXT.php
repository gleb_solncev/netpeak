<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:24
 */


namespace App\Model\File\TypeSave;



class SaveTXT implements iSave
{
    public function doSave($path, $filename, $content)
    {
        $fullPath = $path . DS . $filename . $this->format();

        $fp = fopen($fullPath, 'w');
        file_put_contents($fullPath, is_array($content) ? json_encode($content) : $content);

        fclose($fp);
        return realpath($fullPath);
    }

    public function format(): string
    {
        return '.txt';
    }
}