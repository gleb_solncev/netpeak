<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:16
 */


namespace App\Model;


class Link extends BaseModel
{
    /**
     * @var string $link
     */
    protected $link;

    /**
     * @var array|null $curl_info
     */
    private $curl_info;

    public $content;

    public $baseURL;

    public function checkLink($link, bool $base = false)
    {
        $this->link = $link;

        $this->checkCURL();

        if ($base) $this->baseURL();
    }

    public function checkCURL()
    {
        $ch = curl_init($this->link);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $this->content = curl_exec($ch);
        $this->curl_info = curl_getinfo($ch);
        curl_close($ch);
    }

    /**
     * @param string $find - http_code, protocol, ssl_verifyresult, scheme, total_time
     * @return bool|mixed
     */
    public function get(string $find): string
    {
        return isset($this->curl_info[$find]) ? $this->curl_info[$find] : false;
    }

    protected function baseURL()
    {
        $scheme = strtolower($this->get('scheme'));
        preg_match_all('/^http.*:\/\//m', $this->link, $link);

        # Ставлю правильный протокол
        $link = $this->implodeLink($scheme,
            (reset($link) ? $this->link : $scheme . "://" . $this->link)
        );

        $this->baseURL = $link;
    }

    protected function implodeLink(string $scheme, string $link)
    {
        $toScheme = "://";
        return implode($toScheme, [$scheme, parse_url($link, PHP_URL_HOST)]);
    }

    public function linkRefresh($link)
    {
        $collectionToReplace = array_merge([$this->baseURL], parse_url($this->baseURL));
        $link = str_replace($collectionToReplace, '', $link);

        $path = parse_url($link, PHP_URL_PATH);
        $query = parse_url($link, PHP_URL_QUERY);

        return $this->baseURL . $path . ($query ? "?" . $query : null);
    }

}