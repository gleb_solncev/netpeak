<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 19.01.2020 02:35
 */


namespace App\Model\File\Pattern;


class TitlePattern implements iPattern
{

    public function pattern()
    {
        return '/<title[^>]*>([\s\S]*?)<\/title>/m';
    }
}