<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 21:05
 */


namespace App\Controller;


use App\Model\File;
use App\Model\File\Pattern\HyperLinkPattern;
use App\Model\File\Pattern\ImagePattern;
use App\Model\File\Pattern\iPattern;
use App\Model\File\TypeSave\iSave;
use App\Model\File\TypeSave\SaveCSV;
use App\Model\Link;
use App\Support\Collection;
use App\Support\Logger;
use App\View\cliView;
use App\View\iView;

class ParserController extends BaseController
{

    /**
     * Назвниае папки которая будет создана в процессе окончания анализа
     * @var string $reportDirName
     */
    private $reportDirName = 'reports';

    /**
     * Система логирования.
     * Создает файл с информацией.
     * Если включена константа LOG_DISPLAY отображает ход работы парсинга.
     * Данный класс добавлен лиш для того, чтобы можно было видеть ход работы(тестирования)
     * ОН очень обрезанный. Только для вывода информации для разработчику (БЕЗ ОШИБОК)
     * @var Logger LOG
     */
    private $LOG;

    /**
     * @var Link $link - Модел Линк
     * @var string $baseURL - Базовая ссылка
     * @var string $pathFile - Путь к файлу Отчета
     */
    protected $link, $pathFile;

    /**
     * Класс выполняющий функции парсинга исходника.
     * @var File $file
     */
    private $file;
    /**
     *
     * @var array $collectionLinks - Полный список ссылок
     * @var array $checkedLink - Пройденые ссылки
     * @var array $collectionData - Коллекция ссылки и информации
     */
    protected $collectionLinks = [], $checkedLink = [], $collectionData = [];

    /**
     * ParserController constructor.
     * @param $link
     */
    public function __construct()
    {
        $this->file = new File;
        $this->link = new Link;
        $this->LOG = new Logger(LOG_DISPLAY);

    }

    public function start($link)
    {
        $this->link->checkLink($link, true);
//        $link = $this->link->linkRefresh($link);
//        var_dump($link);die();

        $this->checkedLink = new Collection($link);

        $this->log("1) Сканирование ссылок. ");

        $this->parseLinkPages(); # Сканер всех ссылок по заданой с повторителем

        $this->log("2) Поиск элементов. ".ImagePattern::class);

//        $this->parseEntity(new TitlePattern, false); # Пример расширения парсера
        $this->parseEntity(new ImagePattern, true); # Поиск изображений

        $this->log("3) Создать и сохранить файл. ".SaveCSV::class);
//$this->saveData(new TXT); # Пример расширения сохранения в другой файл.
        $this->saveData(new SaveCSV, 'report'); # Сохранение файл в формате CSV.

//$this->view(new API); # Пример отображения данных в виде JSON
        $this->view(new cliView); # Отображение данных как в ТЗ
    }

    /**
     * @param $text
     * @param null $exit
     */
    public function log($text, $exit = null): void
    {
        if ($exit) $this->LOG->exit($text);
        $this->LOG->print($text);
    }

    /**
     * Первоначальный парсер. Который собирает информацию по первой ссылке.
     * Для начала нам нужно получить список ссылок.
     * После чего их рекурсивно пробежать и записать.
     */
    public function parseLinkPages(): void
    {
        $linkStatus = $this->getLinkData($this->link->get('url'), true);
        if (!$linkStatus) $this->log('Извините, но ссылка не работает', true);

        // HyperLinkPattern - Изначальный класс для получения коллекции ссылок.
        $links = $this->getParsedData(
            new HyperLinkPattern,
            $this->link->content,
            [$this->link->get('url')]
        );

        // чекаем остальные и перепроверяем
        while ($links) $links = $this->recursiveParse($links);
    }

    /**
     * Рекурсивный метод.
     * Запускается с parseLinkPages и собирает активные ссылки с остальных источников.
     * Собирает в коллекцию.
     * Нужен для первоначальной работы.
     *
     * @param $links
     * @return array
     */
    protected function recursiveParse($links): array
    {
        $collection = []; # Нужен для генерации колекции ссылок.
        $this->collectionLinks = array_merge($links, $this->collectionLinks);

        foreach ($links as $link):
            $link = $this->link->linkRefresh($link);
            $linkStatus = $this->getLinkData($link);// Информация о ссылке
            if (!stristr($link, parse_url($this->link->baseURL, PHP_URL_HOST)) or !$linkStatus) continue; // Чек ссылки

            $this->checkedLink->add($link);
            $collection = $this->getParsedData(new HyperLinkPattern, $this->link->content, $collection);// Получаем список ссылок

            // В целях тестинга.
            $this->log(__METHOD__ . " | LINK >>> " . $link . " | " . $this->link->get('http_code'));
            if (SHORT_WORK) {
                $this->log($this->checkedLink->count() . "Количество ссылок для обработки: " . SHORT_WORK);
                if ($this->checkedLink->count() == SHORT_WORK) return [];
            }
        endforeach;

        return array_diff(
            array_unique($collection),
            $this->collectionLinks
        );
    }


    /**
     * Получает ответ от ссылки.
     * Здесь нужно получить ответ 200. Чтобы понимать что ссылка живая
     * Проверка происходит с помощью cURL
     *
     * @param $link
     * @return boolean
     */
    protected function getLinkData($link, bool $baseURL = false)
    {
        $refLink = end(explode('//', $link));
        $this->link->checkLink($refLink, $baseURL);

        return ($this->link->get('http_code') == 200) ? true : false;
    }

    /**
     * Метод парсинга елементов в исходнике.
     * Получает коллекцию и дополняет ее старой колекцией.
     * Выдавая уникальные значения.
     *
     * @param iPattern $type
     * @param $content
     * @param array $oldCollection
     * @return array
     */
    protected function getParsedData(iPattern $type, $content, $oldCollection = []): array
    {
        $this->file->setContent($content);

        $merged = array_merge(
            $this->file->parse($type),
            $oldCollection
        );

        return array_unique($merged);
    }


    /**
     * Пробегаяюсь по коллекции.
     * Собирает найденные елементы.
     *
     * @param iPattern $type
     */
    public function parseEntity(iPattern $type, bool $isLink = false): void
    {
        $collection = $this->checkedLink->map(function($link) use($type, $isLink){
            $linkData = $this->getLinkData($link);
            if (!$linkData) return null;

            $arrayData = $this->getParsedData($type, $this->link->content);
            $data = $this->refreshLinkByArrayData($arrayData, $isLink);


            $this->log(__METHOD__ . " | ENTITY >>> " . $data);
            if ($this->collectionData) $this->refreshCollection($link, $data);

            return compact('link', 'data');
        })->toArray();

        if (!$this->collectionData) $this->collectionData = $collection;
    }

    public function refreshLinkByArrayData($data, $isLink)
    {
        if(!$isLink) return json_encode($data);

        foreach ($data as $url):
            $path = parse_url($url, PHP_URL_PATH);
            $array[] = $this->link->baseURL . $path;
        endforeach;

        return json_encode($array);
    }

    /**
     * Используется в ситуациях когда мы используем несколько елементов.
     * в файле parse - Можно добавить:
     * $parser->parseEntity(new ImagePattern);
     * И парсер добавит еще одну колонку в будущий файл-отчет
     *
     * @param $link
     * @param $data
     */
    protected function refreshCollection($link, $data): void
    {
        $links = array_column($this->collectionData, 'link');
        $key = array_search($link, $links);
        $this->collectionData[$key][] = $data;
    }

    /**
     * Создание файл-отчета.
     * Выполняется реализация пути для будущего файла
     * также в parse - указывается название файла и формат
     *
     * @param iSave $iSave
     * @param string $filename
     */
    public function saveData(iSave $iSave, string $filename)
    {
        $this->file->setContent($this->collectionData);
        $basePath = $this->getBasePath();
        $this->pathFile = $this->file->put($iSave, $basePath, $filename);

        $this->LOG->saveData();
    }

    /**
     * Реализация нового пути.
     * Определяю будущую папку для хранения отчета.
     *
     * @return string
     */
    protected function getBasePath()
    {
        $parseBaseDir = parse_url($this->link->baseURL, PHP_URL_HOST);
        $nameBaseDir = str_replace(['/', '\\'], '', $parseBaseDir);# Для создания папочки проекта
        return $this->reportDirName . DS . $nameBaseDir;
    }

    /*
     * Открываю Вью для
     */
    public function view(iView $option)
    {
        $option->parser($this->pathFile);
    }
}
