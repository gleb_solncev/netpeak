<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:15
 */


namespace App\Model\File\TypeOpen;


interface iOpen
{
    public function open($path);
}