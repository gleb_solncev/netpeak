<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 18.01.2020 00:20
 */


namespace App\Controller;


use App\View\iView;

class HelpController
{

    public function view(iView $option)
    {
        $option->help();
    }
}