<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:11
 */


namespace App\Model\File\TypeSave;


class SaveCSV implements iSave
{
    public function doSave($path, $filename, $content)
    {
        $fullPath = $path . DS . $filename . $this->format();

        $fp = fopen($fullPath, 'w');

        foreach ($content as $fields) fputcsv($fp, $fields);

        fclose($fp);
        return realpath($fullPath);
    }

    public function format(): string
    {
        return '.csv';
    }
}