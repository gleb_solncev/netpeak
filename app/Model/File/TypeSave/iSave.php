<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:11
 */


namespace App\Model\File\TypeSave;


interface iSave
{
    public function format();
    public function doSave($path, $filename, $content);
}