<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:20
 */


namespace App\View;


interface iView
{
    public function parser($path);
    public function report($content);
    public function help();
}