<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 18.01.2020 00:21
 */


namespace App\Controller;


use App\Model\File;
use App\Model\File\TypeOpen\iOpen;
use App\View\iView;

class ReportController
{

    /**
     * @var File $file
     */
    protected $file;

    /**
     * @var string $domain
     * @var string $path
     */
    protected $domain, $path;

    /**
     * @var string $collection
     */
    protected $collection;

    public function __construct($domain)
    {
        $this->file = new File;
        $this->domain = $domain;
        $this->setPath();
    }

    /**
     * Определение с путем
     */
    public function setPath(): void
    {
        $pathToReports = realpath(getcwd() . DS . "reports" . DS);

        $dirs = array_filter(scandir($pathToReports), function ($item) {
            return !is_dir($item);
        });

        if (in_array($this->domain, $dirs)) {
            $key = array_search($this->domain, $dirs);
            $this->path = realpath($pathToReports . DS . $dirs[$key]);
        }
    }

    /**
     * Получение файла
     *
     * @param $filename
     * @param iOpen $open
     */
    public function open($filename, iOpen $open): void
    {
        $this->collection = $open->open($this->path . DS . $filename);
    }


    /**
     * Вывод информации.
     *
     * @param iView $option
     */
    public function view(iView $option)
    {
        $option->report($this->collection);
    }
}