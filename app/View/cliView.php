<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:20
 */


namespace App\View;


class cliView implements iView
{

    private $command_width = 25;
    private $desc_with = 50;


    public function parser($path)
    {
        $result_text[] = "Ссылка на файл с результатами анализа:";
        $result_text[] = $path;

        print  implode(PHP_EOL, $result_text);
    }

    public function report($content)
    {
        $link_w = 100;
        $entity_w = 4;
        $mask = "%-" . $link_w . "s |%-" . $entity_w . "s \n";

        $summ = 0;
        foreach ($content as $item) {
            $summ += array_sum($item['count']);

            printf($mask,
                $this->mb_str_pad(trim($item['link']), $link_w),
                $this->mb_str_pad(implode(' | ', $item['count']) , $entity_w)
            );
        }

        print PHP_EOL . "Всего ссылок: " . count($content);
        print PHP_EOL . "Всего Элементов: " . $summ;
    }

    public function help()
    {
        $command_width = 60;
        $desc_with = 40;

        $mask = "%-" . $command_width . "s |%-" . $desc_with . "s \n";

        $commands = [
            "Команда" => "Описание",
            "parse \t [LINK] [--(log|short=<int>)]" => "Парсинг страницы. С результатами.".
                PHP_EOL."\t [--log=true] - Подключает вывод сообщений парсинга.".
                PHP_EOL."\t [--short=<INT>] - Ограничение по количеству ссылок парсинга",
            "report \t [DOMAIN]" => "Результат парсинга сайта",
            "help" => "Помощь"
        ];

        foreach ($commands as $command => $desc):
            printf($mask,
                $this->mb_str_pad($command, $command_width),
                $this->mb_str_pad($desc, $desc_with)
            );
        endforeach;
    }


    /**
     * Источник: https://www.php.net/manual/ru/function.str-pad.php
     * Проблема в том, что не существует str_pad для Кирилицы, выход тольк такой
     * @param $str
     * @param $pad_len
     * @param string $pad_str
     * @param int $dir
     * @param null $encoding
     * @return string
     */
    function mb_str_pad($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT, $encoding = NULL)
    {
        $encoding = $encoding === NULL ? mb_internal_encoding() : $encoding;
        $padBefore = $dir === STR_PAD_BOTH || $dir === STR_PAD_LEFT;
        $padAfter = $dir === STR_PAD_BOTH || $dir === STR_PAD_RIGHT;
        $pad_len -= mb_strlen($str, $encoding);
        $targetLen = $padBefore && $padAfter ? $pad_len / 2 : $pad_len;
        $strToRepeatLen = mb_strlen($pad_str, $encoding);
        $repeatTimes = ceil($targetLen / $strToRepeatLen);
        $repeatedString = str_repeat($pad_str, max(0, $repeatTimes)); // safe if used with valid utf-8 strings
        $before = $padBefore ? mb_substr($repeatedString, 0, floor($targetLen), $encoding) : '';
        $after = $padAfter ? mb_substr($repeatedString, 0, ceil($targetLen), $encoding) : '';
        return $before . $str . $after;
    }
}