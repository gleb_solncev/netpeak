<?php
/**
 * netpeak
 * Created by: 5-HT.
 * Date: 17.01.2020 23:09
 */


namespace App\Model\File\Pattern;


class HyperLinkPattern implements iPattern
{
    public function pattern()
    {
        return '/<a\s.*?href="(.+?)".*?>/m';
    }
}